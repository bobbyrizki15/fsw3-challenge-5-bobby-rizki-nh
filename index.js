const express = require('express');
const app = express();
const PORT = process.env.PORT || 3200;

app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/public'));

app.get("/", (req, res) => {
    res.render('index')
})

app.listen(PORT, () => {
    console.log(`Server nyala di http://localhost:${PORT}`);
});
